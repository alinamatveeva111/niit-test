#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include "circle.h"
#define N 6378.1//радиус Земли

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbR_clicked();

    void on_pbF_clicked();

    void on_pbA_clicked();

   void on_pbTask1_clicked();

   void on_pbTask2_clicked();

private:
    Ui::MainWindow *ui;
    Circle cir;
};

#endif // MAINWINDOW_H
